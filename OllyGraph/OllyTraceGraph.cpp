// OllyTraceGraph - OllyTraceGraph.c

// OllyTraceGraph is a modification made by Jan Beck of OllyGraph by Austyn Krutsinger

// Thanks for providing the source, Austyn!
// https://github.com/akrutsinger/OllyGraph

// I don't claim any additional copyright for the modifications.

// From the original OllyGraph:

/*******************************************************************************
 * OllyGraph - OllyGraph.c
 *
 * Copyright (c) 2013, Austyn Krutsinger
 * All rights reserved.
 *
 * OllyGraph is released under the New BSD license (see LICENSE.txt).
 *
 ******************************************************************************/

/*
 * VERY IMPORTANT NOTICE: PLUGINS ARE UNICODE LIBRARIES! COMPILE THEM WITH BYTE
 * ALIGNMENT OF STRUCTURES AND DEFAULT UNSIGNED CHAR!
 */

/*******************************************************************************
 * Things to change as I think of them...
 * [ ] = To do
 * [?] = Might be a good idea?
 * [!] = Implemented
 * [+] = Added
 * [-] = Removed
 * [*] = Changed
 * [~] = Almost there...
 *
 * Version 0.2.0 (28AUG2014)
 * [+] Use C++ for unlimited nodes
 * [*] Fixed memory leaks
 *
 * Version 0.1.0 (17MAR2013)
 * [+] Initial release
 *
 *
 * -----------------------------------------------------------------------------
 * TODO
 * -----------------------------------------------------------------------------
 *
 * [ ] Finish
 *
 ******************************************************************************/

/* Defines */
/*
 * Microsoft compilers hate (and maybe justifiably) old-school functions like
 * wcscpy() that may cause buffer overflow and all related dangers. Still, I
 * don't want to see all these bloody warnings.
 */
//#define _CRT_SECURE_NO_DEPRECATE
//#define _CRT_SECURE_NO_WARNINGS
//#define WIN32_LEAN_AND_MEAN     /* Remove extra windows.h information */

/* Includes */
#include <Windows.h>
#include <ShellAPI.h>
#include <time.h>
#include <stdio.h>
#include <string>
#include <list>
#include <set>

#include "plugin.h"
#include "OllyTraceGraph.h"
#include "FunctionGraph.h"
#include "settings.h"
#include "resource.h"

/* Globals Definitions - Module specific */
static HINSTANCE    main_plugin_instance = NULL;    /* Instance of plugin DLL */


/**
 * @display_about_message
 *
 *      Displays "About" message box
 */
void display_about_message(void)
{
    wchar_t about_message[TEXTLEN] = { 0 };
    wchar_t buf[SHORTNAME];
    int n;

    /* Debuggee should continue execution while message box is displayed. */
    Resumeallthreads();
    /* In this case, swprintf() would be as good as a sequence of StrcopyW(), */
    /* but secure copy makes buffer overflow impossible. */
    n = StrcopyW(about_message, TEXTLEN, L"OllyTraceGraph v");
    n += StrcopyW(about_message + n, TEXTLEN - n, PLUGIN_VERS);
    n += StrcopyW(about_message + n, TEXTLEN - n, L"\nOllyGraph Originally coded by OllyGraph by Austyn Krutsinger <akrutsinger@gmail.com>");
    n += StrcopyW(about_message + n, TEXTLEN - n, L"\nAdditions for OllyTraceGraph by Jan Beck <janbeck@gmail.com>");
    n += StrcopyW(about_message + n, TEXTLEN - n, L"\nFixes for OllyTraceGraph by Mr. eXoDia <mr.exodia.tpodt@gmail.com>");
    /* The conditionals below are here to verify that this plugin can be */
    /* compiled with all supported compilers. They are not necessary in the */
    /* final code. */
    n += StrcopyW(about_message + n, TEXTLEN - n, L"\n\nCompiled on ");
    Asciitounicode(__DATE__, SHORTNAME, buf, SHORTNAME);
    n += StrcopyW(about_message + n, TEXTLEN - n, buf);
    n += StrcopyW(about_message + n, TEXTLEN - n, L" ");
    Asciitounicode(__TIME__, SHORTNAME, buf, SHORTNAME);
    n += StrcopyW(about_message + n, TEXTLEN - n, buf);
    n += StrcopyW(about_message + n, TEXTLEN - n, L" with ");
#if defined(__BORLANDC__)
    n += StrcopyW(about_message + n, TEXTLEN - n, L"Borland (R) ");
#elif defined(_MSC_VER)
    n += StrcopyW(about_message + n, TEXTLEN - n, L"Microsoft (R) ");
#elif defined(__MINGW32__)
    n += StrcopyW(about_message + n, TEXTLEN - n, L"MinGW32 ");
#else
    n += StrcopyW(about_message + n, TEXTLEN - n, L"\n\nCompiled with ");
#endif
#ifdef __cplusplus
    StrcopyW(about_message + n, TEXTLEN - n, L"C++ compiler");
#else
    StrcopyW(about_message + n, TEXTLEN - n, L"C compiler");
#endif
    MessageBox(hwollymain, about_message, L"About OllyGraph", MB_OK | MB_ICONINFORMATION);
    /* Suspendallthreads() and Resumeallthreads() must be paired, even if they */
    /* are called in inverse order! */
    Suspendallthreads();
}

/**
 * @menu_handler
 *
 *      Menu callback for our plugin to process our menu commands.
 */
int menu_handler(t_table* pTable, wchar_t* pName, ulong index, int nMode)
{
    UNREFERENCED_PARAMETER(pTable);
    UNREFERENCED_PARAMETER(pName);

    switch(nMode)
    {
    case MENU_VERIFY:
        return MENU_NORMAL;

    case MENU_EXECUTE:
        switch(index)
        {
        case MENU_SETTINGS: /* Menu -> Settings */
            DialogBox(main_plugin_instance,
                      MAKEINTRESOURCE(IDD_SETTINGS),
                      hwollymain,
                      (DLGPROC)settings_dialog_procedure);
            break;
        case MENU_FUNCTION_FLOWCHART: /* Menu | Disasm Menu -> Generate Function Flowchart */
            generate_function_flowchart();
            break;
        case MENU_FUNCTION_CALL_GRAPH: /* Menu | Disasm Menu -> Generate Function Call Graph */
            break;
        case MENU_XREFS_TO_ADDRESS_GRAPH:   /* Menu | Disasm Menu -> Generate XRefs To Address Graph */
            break;
        case MENU_XREFS_FROM_ADDRESS_GRAPH: /* Menu | Disasm Menu -> Generate XRefs From Address Graph */
            break;
        case MENU_ABOUT: /* Menu -> About */
            display_about_message();
            break;
        }
        return MENU_NOREDRAW;
    }

    return MENU_ABSENT;
}

/**
 * @ODBG2_Pluginmenu
 *
 *      Adds items to OllyDbgs menu system.
 */
extc t_menu* __cdecl ODBG2_Pluginmenu(wchar_t* type)
{
    if(wcscmp(type, PWM_MAIN) == 0)
        /* Main menu. */
        return ollygraph_menu;
    else if(wcscmp(type, PWM_DISASM) == 0)
        /* Disassembler pane of CPU window. */
        return ollygraph_popup_menu;
    return NULL;        /* No menu */
};

static ULONG_PTR ollyGetInstrInfo(ULONG_PTR addr, instr_info* info)
{
    ULONG_PTR dsize;
    uchar* decode = Finddecode(addr, &dsize);

    uchar cmdbuf[MAXCMDSIZE];
    Readmemory(cmdbuf, addr, MAXCMDSIZE, MM_SILENT | MM_PARTIAL);

    //append new disasm structure to the list
    t_disasm disasm;
    int cmdsize = Disasm(cmdbuf, MAXCMDSIZE, addr, decode, &disasm, DA_TEXT | DA_OPCOMM | DA_MEMORY, Threadregisters(Getcpudisasmdump()->threadid), NULL);
    if(!cmdsize)
        cmdsize = 1;

    //fill instr_info
    info->addr = disasm.ip;
    info->jmpaddr = disasm.jmpaddr;
    char temp[TEXTLEN] = "";
    Unicodetoascii(disasm.result, TEXTLEN, temp, TEXTLEN);
    info->instrText = temp;
    Unicodetoascii(disasm.comment, TEXTLEN, temp, TEXTLEN);
    info->comment = temp;

    return addr + cmdsize;
}

void generate_function_flowchart()
{
    ULONG_PTR start = 0;
    ULONG_PTR end = 0;
    if(Getproclimits(Getcpudisasmdump()->sel0, &start, &end) == -1)
    {
        MessageBox(hwollymain,
                   L"Address is not within known function boundaries\n"
                   L"(Did you run Analyze Code?)",
                   L"Function boundaries not found", MB_OK | MB_ICONEXCLAMATION);
        return;
    }

    wchar_t path_buffer[DATALEN];
    GetTempPath(DATALEN, path_buffer);
    cleanup_tempfiles(path_buffer);
    wchar_t temp_name[MAX_PATH];
    GetTempFileName(path_buffer, L"ogh", 0, temp_name);

    if(!make_flowchart(start, end, temp_name, ollyGetInstrInfo))
    {
        MessageBox(hwollymain,
                   L"Failed to create temporary file!",
                   L"Create Temp File Failed", MB_OK | MB_ICONEXCLAMATION);
    }
    else if((int)ShellExecute(hwollymain, L"open", global_qwingraph_path, temp_name, NULL, SW_SHOWNORMAL) < 32)
    {
        Error(L"Error while executing %s\n(check qwingraph path configuration)", global_qwingraph_path);
    }
}

void generate_function_call_graph(void)
{
    // not implemented
}

void generate_xrefs_to_address_graph(void)
{
    // not implemented
}

void generate_xrefs_from_address_graph(void)
{
    // not implemented
}

void cleanup_tempfiles(wchar_t* tmppath)
{
    WIN32_FIND_DATA FindFileData;
    HANDLE hFind;
    wchar_t DirSpec[MAX_PATH];
    wchar_t FoundFile[MAX_PATH];
    DWORD dwError;

    StrcopyW(DirSpec, MAX_PATH, tmppath);
    wcsncat_s(DirSpec, L"ogh*.tmp", 8);
    hFind = FindFirstFile(DirSpec, &FindFileData);

    if(hFind == INVALID_HANDLE_VALUE)
    {
        return;
    }
    else
    {
        StrcopyW(FoundFile, MAX_PATH, tmppath);
        wcscat_s(FoundFile, FindFileData.cFileName);
        DeleteFile(FoundFile);
        while(FindNextFile(hFind, &FindFileData) != 0)
        {
            StrcopyW(FoundFile, MAX_PATH, tmppath);
            wcscat_s(FoundFile, FindFileData.cFileName);
            DeleteFile(FoundFile);
        }

        dwError = GetLastError();
        if(dwError == ERROR_NO_MORE_FILES)
        {
            FindClose(hFind);
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
//////////////////////////// PLUGIN INITIALIZATION /////////////////////////////

/**
 * @DllMain
 *
 *      Dll entrypoint - mainly unused.
 */
BOOL WINAPI DllMain(HINSTANCE hinstDll, DWORD fdwReason, LPVOID lpReserved)
{
    if(fdwReason == DLL_PROCESS_ATTACH)
        main_plugin_instance = hinstDll;        /* Save plugin instance */
    return 1;                           /* Report success */
};

/**
 * @ODBG2_Pluginquery - required!
 *
 *      Handles initializing the plugin.
 */
extc int __cdecl ODBG2_Pluginquery(int ollydbgversion, ULONG_PTR* features, wchar_t pluginname[SHORTNAME], wchar_t pluginversion[SHORTNAME])
{
    /*
     * Check whether OllyDbg has compatible version. This plugin uses only the
     * most basic functions, so this check is done pro forma, just to remind of
     * this option.
     */
    if(ollydbgversion < 201)
        return 0;
    /* Report name and version to OllyDbg */
    StrcopyW(pluginname, SHORTNAME, PLUGIN_NAME);
    StrcopyW(pluginversion, SHORTNAME, PLUGIN_VERS);
    return PLUGIN_VERSION;          /* Expected API version */
};

/**
 * @ODBG2_Plugininit - optional
 *
 *      Handles one-time initializations and allocate resources.
 */
extc int __cdecl ODBG2_Plugininit(void)
{
    int ret = 0;

    load_settings(NULL);

    Addtolist(0, DRAW_NORMAL, L"");
    Addtolist(0, DRAW_NORMAL, L"[*] %s v%s", PLUGIN_NAME, PLUGIN_VERS);
    Addtolist(0, DRAW_NORMAL, L"[*] Coded by: Austyn Krutsinger <akrutsinger@gmail.com>");
    Addtolist(0, DRAW_NORMAL, L"");

    /* Report success. */
    return 0;
};

/*
 * Optional entry, called each time OllyDbg analyses some module and analysis
 * is finished. Plugin can make additional analysis steps. Debugged application
 * is paused for the time of processing. Bookmark plugin, of course, does not
 * analyse code. If you don't need this feature, remove ODBG2_Pluginanalyse()
 * from the plugin code.
 */
extc void __cdecl ODBG2_Pluginanalyse(t_module* pmod)
{

};

/*
 * OllyDbg calls this optional function once on exit. At this moment, all MDI
 * windows created by plugin are already destroyed (and received WM_DESTROY
 * messages). Function must free all internally allocated resources, like
 * window classes, files, memory etc.
 */
extc void __cdecl ODBG2_Plugindestroy(void)
{

};

/*
 * Function is called when user opens new or restarts current_addr application.
 * Plugin should reset internal variables and data structures to the initial
 * state.
 */
//extc void __cdecl ODBG2_Pluginreset(void)
//{
//};

/*
 * OllyDbg calls this optional function when user wants to terminate OllyDbg.
 * All MDI windows created by plugins still exist. Function must return 0 if
 * it is safe to terminate. Any non-zero return will stop closing sequence. Do
 * not misuse this possibility! Always inform user about the reasons why
 * termination is not good and ask for his decision! Attention, don't make any
 * unrecoverable actions for the case that some other plugin will decide that
 * OllyDbg should continue running.
 */
//extc int __cdecl ODBG2_Pluginclose(void)
//{
//  return 0;
//};


////////////////////////////////////////////////////////////////////////////////
/////////////////////////// EVENTS AND NOTIFICATIONS ///////////////////////////
/*
 * If you define ODBG2_Pluginmainloop(), this function will be called each time
 * from the main Windows loop in OllyDbg. If there is some (real) debug event
 * from the debugged application, debugevent points to it, otherwise it's NULL.
 * If fast command emulation is active, it does not receive all (emulated)
 * exceptions, use ODBG2_Pluginexception() instead. Do not declare these two
 * functions unnecessarily, as this may negatively influence the overall speed!
 */
// extc void __cdecl ODBG2_Pluginmainloop(DEBUG_EVENT *debugevent) {
// };
// extc void __cdecl ODBG2_Pluginexception(t_run *prun, t_thread *pthr, t_reg *preg) {
// };

/*
 * Optional entry, notifies plugin on relatively infrequent events.
 */
extc void __cdecl ODBG2_Pluginnotify(int code, void* data, ULONG_PTR parm1, ULONG_PTR parm2)
{
    if(code == PN_NEWPROC)
    {
        //      if (global_scan_on_mod_load == TRUE)
        //          scan_module();
    }
};